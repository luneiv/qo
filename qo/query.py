import textwrap
import tempfile
import os
import subprocess

def expand_to_sqlplus(sql, output_file=None):
    stmt = textwrap.dedent('''
    set markup csv on;
    set termout off;
    set feedback off;
    SET SQLBLANKLINES ON;
    ''')
    if output_file is not None:
        stmt += f"spool '{output_file}';\n";
        stmt += '\n' + sql + '\n\n'
        stmt += 'spool off;\n'
    else:
        stmt += '\n' + sql + '\n\n'
    stmt += 'quit;\n'

    return stmt

def execute(sqlplus_stmt, conn_string):
    filename = tempfile.mktemp(suffix='.sql')
    print(filename, os.path.isfile(filename))
    print(sqlplus_stmt)
    with open(filename, 'w') as f:
        f.write(sqlplus_stmt)
    print(filename, os.path.isfile(filename))
    try:
        print(['sqlplus', conn_string, f"@{filename}"])
        subprocess.run(['sqlplus', conn_string, f"@{filename}"])
    finally:
        os.remove(filename)

def create_connection_string(usr, psw, db_schema):
    return f"{usr}/{psw}@{db_schema}"