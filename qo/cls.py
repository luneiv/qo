"""Naval Fate.

Usage:
  qo ORACLE_LOGIN OUT_CSV [(-q sql | -f sqlf)]
  qo (-h | --help)
  qo --version

Options:
  -h --help             Show this screen.
  -q --query sql        The sql query string. Remember to quote.
  -f --query-file sqlf  A file containing an sql query.
  -v --version          Print version information.

"""
from docopt import docopt
import os

from .query import expand_to_sqlplus, execute

def get_query(qfile, query):
  if  qfile is not None:
      assert os.path.isfile(qfile)
      with open(qfile) as f:
        return f.read()
  elif query is not None:
    return query
  raise RuntimeError('No query defined')


def main():
    arguments = docopt(__doc__, version='qo 0.1')
    query = get_query(arguments['--query-file'], arguments['--query'])
    print(query)
    sqlplus = expand_to_sqlplus(query, output_file=arguments['OUT_CSV'])
    execute(sqlplus, arguments['ORACLE_LOGIN'])

if __name__ == '__main__':
    main()