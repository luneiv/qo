# QO - Query Oracle

Store the result of a SQL query to an Oracle database as a CSV file.

Oracle does not, as far as I know, have an easy to use command line tool that executes a query against an Oracle databse and stores the result in a file.

This tool wraps the SQLPLUS tool from oracle and provides an easier to use interface.

## Installation
Clone repo and install using pip.

## Usage
```
qo 'luneiv/<my-psw>@dwprd.gard.local' ratetool.csv -q "SELECT * FROM STAGE_LLOYDS.lloyds_ratetool;"
```