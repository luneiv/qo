import qo.query

import textwrap

def test_sql_to_sqlplus():
    expect = textwrap.dedent('''
    set markup csv on;
    set termout off;
    set feedback off;
    SET SQLBLANKLINES ON;

    TEST

    quit;
    ''')
    actual = qo.query.expand_to_sqlplus('TEST')
    # print(expect)
    # print(actual)
    assert expect == actual

def test_sql_to_sqlplus_output_file():
    expect = textwrap.dedent('''
    set markup csv on;
    set termout off;
    set feedback off;
    SET SQLBLANKLINES ON;
    spool 'outfile.csv';

    TEST

    spool off;
    quit;
    ''')
    actual = qo.query.expand_to_sqlplus('TEST', output_file='outfile.csv')
    # print(expect)
    # print(actual)
    assert expect == actual