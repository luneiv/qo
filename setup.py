from setuptools import setup

setup(
    name="qo",
    version="0.1",
    description="""Query oracle - run sql query against an oracle database.

      Run sql against an oracle database. Return data in CSV format. Optionally store to file.
      """,
    # url='http://github.com/storborg/funniest',
    author="Eivind Gard Lund",
    author_email="eivind.lund@gard.no",
    license="MIT",
    packages=["qo"],
    install_requires=["docopt"],
    entry_points={"console_scripts": ["qo=qo.cls:main"]},
    zip_safe=False,
)
